﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormationSNCF_ETUDIANT.Modele;
using FormationSNCF_ETUDIANT.Ressources;
using FormationSNCF_ETUDIANT.Vues;



namespace FormationSNCF.Vues
{
    public partial class FormFenetrePrincipale : Form
    {
        private Form _mdiChild;
        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        } 

        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionLieux();
        } 

        private void FormFenetrePrincipaleForm_Closing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
       
        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }

        private void AjouterUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormAjouterAgent();
        }
    }
}
