﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormationSNCF_ETUDIANT.Modele;
using FormationSNCF_ETUDIANT.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionLieux : Form
    {
        public FormGestionLieux()
        {
            InitializeComponent();
        }

        private void FormGestionLieu_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Donnees.CollectionLieu;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonAjoutLieu_Click(object sender, EventArgs e)
        {
            if (textBoxLibelle.Text.Length < 3)
            {
                MessageBox.Show("Le lieu doit etre composé d'au moins 3 caractères");
                textBoxLibelle.Text = "";
                textBoxLibelle.Focus();
            }

            else if (textBoxCodePostal.Text.Length == 0)
            {
                MessageBox.Show("Le numéro de téléphone est obligatoire");
                textBoxCodePostal.Focus();
            }

            else if (textBoxTelephone.Text.Length == 0)
            {
                MessageBox.Show("Le code postal est obligatoire");
                textBoxTelephone.Focus();
            }

            else
            {
                dataGridView1.DataSource = null;
                int numeroMax = 0;
                foreach(Lieu lieuCourant in Donnees.CollectionLieu)
                {
                    if (lieuCourant.Numero > numeroMax)
                        numeroMax = lieuCourant.Numero;
                }

                Lieu unLieu = new Lieu(numeroMax + 1, textBoxLibelle.Text, textBoxCodePostal.Text, textBoxTelephone.Text);
                Donnees.CollectionLieu.Add(unLieu);
                dataGridView1.DataSource = Donnees.CollectionLieu;
            }
        }

        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (textBoxCodePostal.Text.Length !=0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodePostal.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffres");
                    textBoxCodePostal.Text = "";
                    textBoxCodePostal.Focus();
                }
            }
        }

        private void TextBoxTelephone_Leave(object sender, EventArgs e)
        {
            if (textBoxTelephone.Text.Length !=0)
            {
                if (!Formulaire.VerificationTelephone(textBoxTelephone.Text))
                {
                    MessageBox.Show("Saisir un numéro de téléphone à 10 chiffres");
                    textBoxTelephone.Text = "";
                    textBoxTelephone.Focus();
                }
            }
        }
    }
}
