﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF_ETUDIANT.Vues;
using FormationSNCF_ETUDIANT.Modele;
using FormationSNCF_ETUDIANT.Ressources;

namespace FormationSNCF_ETUDIANT.Vues
{
    /// <summary>
    /// Interface qui permet la gestion des actions de formations
    /// </summary>
    public partial class FormGestionActionFormation : Form
    {
        public FormGestionActionFormation()
        {
            InitializeComponent();
        }
      
        private void TextBoxDateCreation_Leave(object sender, EventArgs e)
        {
            if (textBoxDateCreation.Text != "")
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateCreation.Text))
                {
                    MessageBox.Show("Veuillez saisir une date au format jj-mm-aaaa");
                    textBoxDateCreation.Clear();
                    textBoxDateCreation.Focus();
                }
            }
        }

        private void TextBoxDureeAction_Leave(object sender, EventArgs e)
        {
            if (textBoxDureeAction.Text != "")
            {
                if (!Numerique.IsNumerique(textBoxDureeAction.Text))
                {
                    MessageBox.Show("Veuillez saisir une valeur entière");
                    textBoxDureeAction.Clear();
                    textBoxDureeAction.Focus();
                }
            }
        }      
        

    }
}
