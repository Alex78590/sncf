﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FormationSNCF_ETUDIANT.Modele
{
    [Serializable]
    /// <summary>
    /// Classe métier : représente une activité
    /// </summary>
    public class Activite
    {
        private string _libelleActivite;
        private List<ActionFormation> _lesActionsFormation;

        /// <summary>
        /// Obtient le libellé de l'activité
        /// </summary>
        public string LibelleActivite
        {
            get { return _libelleActivite; }
        }
        
        /// <summary>
        /// Initialise une activité
        /// </summary>
        /// <param name="libelleActivite">Le libellé de l'activité</param>
        public Activite(string libelleActivite)
        {
            _libelleActivite = libelleActivite;
            _lesActionsFormation = new List<ActionFormation>();
        }
    }
}
